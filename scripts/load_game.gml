///load_game()
///@desc Loads the game data

//Destroy whatever data has been changed since our last save to prevent memory leak
//(loading with a bow in inventory obtained before the last save)
//Then recreate the variable with the previous save data
ds_map_destroy(global.SAVE_DATA);
global.SAVE_DATA = ds_map_secure_load("SaveData.sav");

//All these variables should be human readable and indicate what they load
//While all these variables should be defined, it's good practice to check anyways
//(Defining happens during the initialization of the save slot in name registration)
var hp = ds_map_find_value(global.SAVE_DATA,"HP");
if(!is_undefined(hp))
    obj_stats.hp = hp;
