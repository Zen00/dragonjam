///add_screenshake(amount,duration)
var amount = argument0;
var duration = argument1;

obj_camera.screenshake = amount;
obj_camera.alarm[0] = duration;
