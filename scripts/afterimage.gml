///afterimage(obj,ttl)
/// argument0 = object that needs the afterimage
/// argument1 = frames to live

// assumes an object named fx_afterimage
// fx_afterimage has a varible named "my_fade", set to 0.5 in the create
// In step after, fx_afterimage has "image_alpha = image_alpha - my_fade"
// in step before, "if image_alpha <= 0 then instance_destroy()";


my_afterimage = noone
with (argument0)
    {
        with other {
        my_afterimage = instance_create(other.x,other.y,fx_afterimage)
        my_afterimage.depth = other.depth-1
        my_afterimage.direction = other.direction
        my_afterimage.speed = 0
        my_afterimage.sprite_index = other.sprite_index
        my_afterimage.image_index = other.image_index
        my_afterimage.my_fade = 1/(argument1+1)
        my_afterimage.image_alpha = other.image_alpha - my_afterimage.my_fade
        my_afterimage.image_speed = 0
        my_afterimage.image_angle = other.image_angle
        my_afterimage.image_blend = other.image_blend 
        my_afterimage.image_xscale = other.image_xscale
        my_afterimage.image_yscale = other.image_yscale 
        }
    }

return my_afterimage
