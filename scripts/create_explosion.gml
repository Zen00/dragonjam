///create_explosion(x,y)
var xx = argument0;
var yy = argument1;

repeat(10)
    instance_create(xx-16+random(32),yy-16+random(32),obj_explosion_particle_emitter);

part_particles_create(obj_particles.asteroid_explosions,xx,yy,obj_particles.explosion_center_part,2);
